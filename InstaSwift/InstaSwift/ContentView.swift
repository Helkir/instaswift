//
//  ContentView.swift
//  InstaSwift
//
//  Created by Arnaud Chrétien on 09/07/2020.
//  Copyright © 2020 Arnaud Chrétien. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List {
                StoriesView()
                PostCell()
                PostCell()
                PostCell()
            }.navigationBarTitle("InstaSwift", displayMode: .inline).navigationBarItems(leading: Image("Camera").resizable().frame(width: 20, height: 20), trailing: Image("Send1").resizable().frame(width: 20, height: 20))
        }
    }
}

struct PostCell: View {
    var body: some View {
        VStack {
            // Header
            HStack {
                Image("Paulson").resizable().frame(width: 40, height: 40)
                VStack(alignment: .leading){
                    Text("Robert Paulson")
                        .font(Font.system(size: 13.5))
                    Text("Dunno, USA")
                        .font(Font.system(size: 11.5))
                }
                Spacer()
                Image("More").resizable().frame(width: 20, height: 20)
            }
            
            // Post
            Image("Photo")
                .resizable()
                .scaledToFit()
                .padding(.leading, -20)
                .padding(.trailing, -20)
            
            // Barre horizontale
            HStack(alignment: .center) {
                Image("Like").resizable().frame(width: 30, height: 30)
                Image("Comment").resizable().frame(width: 30, height: 30)
                Image("Send").resizable().frame(width: 30, height: 30)
                Spacer()
                Image("Flag").resizable().frame(width: 30, height: 30)
            }
            
            // Le nombre de Likes
            Text("Liked by leeviahq and 621 others")
                .font(Font.system(size: 13.5))
            
            // La description
            Text("Hello coucou ceci est une description blablablablablablablabla blablablablablal ")
                            .lineLimit(4)
                            .font(Font.system(size: 12.5))
                            .foregroundColor(.init(white: 0.2))
        }
    }
}

struct StoriesView: View {
    var tablo = [1,2,3,4,5,6,7,8,9,10]
    var body: some View {
        VStack(alignment: .leading) {
            
            // Stories text + Watch all
            HStack {
               Text("Stories")
               Spacer()
             //  Image("Watch")
               Text("Watch all")
            }
            
            // Stories Circles
            ScrollView([.horizontal], showsIndicators: false){
                HStack {
                    VStack {
                         ZStack(alignment: .bottomTrailing) {
                              Image("Portrait").resizable().frame(width: 45, height: 45).clipShape(Circle())
                              Image("Add").resizable().frame(width: 15, height: 15)
                         }
                         Text("Your Story")
                             .font(Font.system(size: 13.5))
                    }//.padding(.trailing, 12)
                    
                   ForEach(tablo, id: \.self) { value in
                       makeStory()
                   }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

func makeStory() -> some View {
    return VStack {
         ZStack {
              Image("Avatar").resizable().frame(width: 45, height: 45).clipShape(Circle()).shadow(radius: 1)
              .overlay(Circle().stroke(Color.red, lineWidth: 2))
         }
         Text("The Dude")
             .font(Font.system(size: 13.5))
    }
}
